﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace ProiectTestareAutokarma
{
    public class UserDataModel
    {
        public string nume;
        public string prenume;
        public string telefon;
        public string email;
        public string judet;
        public string localitate;
        public string adresa;
        public string codPostal;
        public string MagazinPreferat;
        public string parola;
        public string confimareParola;

        public UserDataModel()
        {
            nume = "Marcelescu";
            prenume = "Eminovici";
            telefon = "0728898765";
            email = "marcelescu.eminovici@gmail.com";
            judet = "2703"; //Iasi
            localitate = "7874"; //Iasi
            adresa = "Str. Palas, Nr.3";
            codPostal = "700028";
            MagazinPreferat = "6"; //Iasi - Nicolae Iorga 41
            parola = "testParola1234@#$$";
            confimareParola = parola;
        }
    }

    public class RegisterPageModel
    {
        IWebDriver driver;
        IWebElement numeField;
        IWebElement prenumeField;
        IWebElement emailField;
        IWebElement telefonField;
        IWebElement judetDropDownList;
        IWebElement localitateDropDownList;
        IWebElement adresaField;
        IWebElement codPostalField;
        IWebElement magazinulPreferatDropDownList;
        IWebElement parolaField;
        IWebElement confirmareParolaField;
        IWebElement confirmareTAndC;
        IWebElement confirmareGDPR;

        public RegisterPageModel(IWebDriver driver)
        {
            this.driver = driver;
            numeField = driver.FindElement(By.Id("input-lastname"));
            prenumeField = driver.FindElement(By.Id("input-firstname"));
            emailField = driver.FindElement(By.Id("input-email"));
            telefonField = driver.FindElement(By.Id("input-telephone"));
            judetDropDownList = driver.FindElement(By.Id("input-zone"));
            localitateDropDownList = driver.FindElement(By.Id("input-city"));
            adresaField = driver.FindElement(By.Id("input-address-1"));
            codPostalField = driver.FindElement(By.Id("input-postcode"));
            magazinulPreferatDropDownList = driver.FindElement(By.Id("input-location_id"));
            parolaField = driver.FindElement(By.Id("input-password"));
            confirmareParolaField = driver.FindElement(By.Id("input-confirm"));
            confirmareTAndC = driver.FindElement(By.Id("register_agree"));
            confirmareGDPR = driver.FindElement(By.Id("date_agree"));
        }
        public void populateFields(UserDataModel user)
        {
            try
            {
                if(driver != null)
                {
                    numeField.SendKeys(user.nume);
                    prenumeField.SendKeys(user.prenume);
                    emailField.SendKeys(user.email);
                    telefonField.SendKeys(user.telefon);

                    // Select judet din dropdown list
                    SelectElement dropDownList = new SelectElement(judetDropDownList);
                    dropDownList.SelectByValue(user.judet);

                    Thread.Sleep(1500);

                    // Select localitate din dropdown list
                    dropDownList = new SelectElement(localitateDropDownList);
                    dropDownList.SelectByValue(user.localitate);

                    adresaField.SendKeys(user.adresa);
                    codPostalField.SendKeys(user.codPostal);

                    // Magazinul preferat dropdown list
                    dropDownList = new SelectElement(magazinulPreferatDropDownList);
                    dropDownList.SelectByValue(user.MagazinPreferat);

                    Thread.Sleep(1000);

                    parolaField.SendKeys(user.parola);
                    confirmareParolaField.SendKeys(user.confimareParola);

                    // confirmare Termeni si Conditii
                    confirmareTAndC.Click();
                    Assert.AreEqual(true, confirmareTAndC.Selected);

                    // confirmare GDPR
                    confirmareGDPR.Click();
                    Assert.AreEqual(true, confirmareGDPR.Selected);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine($"Eroare populare inregistrare : {e.Message}");
            }
        }
    }

    [TestClass]
    public class AutokarmaRegisterPageTest
    {
        UserDataModel user;
        IWebDriver driver;
        public AutokarmaRegisterPageTest()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = "https://www.autokarma.ro/creare-cont";
            user = new UserDataModel();
        }
        [TestMethod]
        public void TestInregistare()
        {
            RegisterPageModel pageModel = new RegisterPageModel(driver);
            pageModel.populateFields(user);
        }
    }
}
