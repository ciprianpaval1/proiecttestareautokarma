﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;

namespace ProiectTestareAutokarma
{

    [TestClass]
    public class AutokarmaHomePageTest
    {
        private IWebDriver driver;
        private IWebElement closePopUpBtn;

        public AutokarmaHomePageTest()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = "https://www.autokarma.ro";
        }
        /// <summary>
        /// Apasa butonul de accept cookie si verifica daca cookie-ul 'notcookie' este pe true
        /// </summary>
        [TestMethod]
        public void Test1()
        {
            IWebElement acceptCookieBtn = driver.FindElement(By.ClassName("notify_cookie-close"));
            if (acceptCookieBtn != null)
            {
                acceptCookieBtn.Click();
                Thread.Sleep(1000);
                Cookie notcookie = driver.Manage().Cookies.GetCookieNamed("notcookie");
                Assert.AreEqual("1", notcookie.Value);
            }
            else
            {
                Assert.Fail();
                driver.Close();
            }

        }

        /// <summary>
        /// Verifica butonul de selectare a vehiculului si il apasa
        /// </summary>
        [TestMethod]
        public void Test2()
        {
            IWebElement selectVehiclePopupBtn = driver.FindElement(By.ClassName("select-vehicle-popup-btn"));
            if (selectVehiclePopupBtn != null)
            {
                selectVehiclePopupBtn.Click();
                Thread.Sleep(1000);
                IWebElement vehicleDiv = driver.FindElement(By.Id("carfilter-modal"));
                Assert.AreEqual(true, vehicleDiv.Displayed);
                closePopUpBtn = driver.FindElement(By.ClassName("close"));
            }
            else
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// Verifica butonul de inregistrare din drop-downul CONTUL MEU
        /// </summary>
        [TestMethod]
        public void Test3()
        {
            if (closePopUpBtn != null)
            {
                closePopUpBtn.Click();
            }
            IWebElement contulMeuDropDown = driver.FindElement(By.CssSelector("div[class='quick-top-link links pull-right']"));
            Actions action = new Actions(driver);
            action.MoveToElement(contulMeuDropDown).Build().Perform();

            By byXpath = By.XPath("//a[(@id='register-total') and (@class = 'btn btn-inverse')]");
            IWebElement inregistrareButton = driver.FindElement(byXpath);
            Assert.AreEqual(true,inregistrareButton.Enabled);
        }
    }
}
