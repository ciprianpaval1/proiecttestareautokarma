﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;

namespace ProiectTestareAutokarma
{
    [TestClass]
    public class AutokarmaLoginTest
    {
        IWebDriver driver;
        public AutokarmaLoginTest()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = "https://www.autokarma.ro/intrare-cont";
        }
        /// <summary>
        /// Login Test date autentificare incorecte
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            string email = "emailtest@gmail.com";
            string parola = "parolatest1234!@#$";

            IWebElement emailField = driver.FindElement(By.Id("input-email"));
            IWebElement passwordField = driver.FindElement(By.Id("input-password"));

            emailField.SendKeys(email);
            passwordField.SendKeys(parola);

            IWebElement loginBtn = driver.FindElement(By.XPath("//button[(@type='submit') and (@value='login')]"));
            loginBtn.Click();

            Thread.Sleep(2500);

            IWebElement alertIncorectCredentials = driver.FindElement(By.XPath("//div[@class='alert alert-danger']"));
            Assert.AreEqual(true, alertIncorectCredentials.Displayed);

        }
    }
}
