﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Threading;
using System.Windows;

namespace ProiectTestareAutokarma
{
    [TestClass]
    public class AutokarmaChooseCarTest
    {
        private void clickonHref(IWebElement hrefToClick)
        {
            executor.ExecuteScript("arguments[0].click();", hrefToClick);

        }
        IWebDriver driver;
        IJavaScriptExecutor executor;
        public AutokarmaChooseCarTest()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = "https://www.autokarma.ro/piese-auto";
            executor = (IJavaScriptExecutor) driver;
        }
        [TestMethod]
        public void ChooseFordCar()
        {
            By fordXPath = By.XPath("//div[@data-original-title='Catalog piese - Autoturisme & Autoutilitare - FORD']");
            By fordHrefXPath = By.XPath("//a[@href='https://www.autokarma.ro/piese-auto-ford']");
            IWebElement pieseAutoFord = driver.FindElement(fordXPath);
            IWebElement pieseFordHref = pieseAutoFord.FindElement(fordHrefXPath);

            // To to piese auto ford
            clickonHref(pieseFordHref);
            Assert.AreEqual("https://www.autokarma.ro/piese-auto-ford", driver.Url);

            // Go to ford focus

            var fordFocusXPath = By.XPath("//div[@data-original-title='Catalog piese - Autoturisme & Autoutilitare - FORD FOCUS']");
            var fordFocusHrefXPath = By.XPath("//a[@href='https://www.autokarma.ro/piese-auto-ford?grupa_modele=FOCUS']");
            IWebElement pieseFordFocus = driver.FindElement(fordFocusXPath);
            IWebElement pieseFordFocusHref = pieseFordFocus.FindElement(fordFocusHrefXPath);

            clickonHref(pieseFordFocusHref);
            Assert.AreEqual("https://www.autokarma.ro/piese-auto-ford?grupa_modele=FOCUS", driver.Url);

            // To to ford focus II berlina(DA)

            var fordFocusBerlinaHrefXPath = By.XPath("//a[@href='https://www.autokarma.ro/piese-auto-ford-focus-ii-berlina-da-2005']");
            IWebElement pieseFordFocusBerlinaHref = driver.FindElement(fordFocusBerlinaHrefXPath);

            clickonHref(pieseFordFocusBerlinaHref);
            Assert.AreEqual("https://www.autokarma.ro/piese-auto-ford-focus-ii-berlina-da-2005", driver.Url);

            // 1.4 59kw

            IWebElement engineHref = driver.FindElement(By.XPath("//a[@class='107685']"));
            clickonHref(engineHref);
            Assert.AreEqual("https://www.autokarma.ro/piese-auto-ford-focus-ii-berlina-da-2005-1-4-59-kw", driver.Url);

        }
    }
}
